# MANIPULATING VARIABLES

## Multiplication and Division

```java
// 2
int divided = 10/5;

// 2, (2.5) remove 0.5
// int can not store decimal
int divided = 10/4;
```

## Object can't use the primitive equality (`==`) operator

```java
boolean isSame = string1.equals(string2);
```

## Concatenate String

```java
String name = "Chuan";
System.out.println("My name is: " + name);

int age = 23;
System.out.println("age: " + age);
```
