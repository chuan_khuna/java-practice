public class AboutMe {
  public static void main(String[] args) {
    String name = "Chuan";
    int birthYear = 1997;
    double weight = 48.0;
    double height = 1.68;

    int currentYear = 2020;

    int age = currentYear - birthYear;
    double bmi = weight/(height*height);
    boolean isFat = bmi >= 23.0;

    System.out.println("My name is " + name);
    System.out.println("age: " + age);
    System.out.println("BMI: " + bmi + ", " + "fat?: " + isFat);
  }
}