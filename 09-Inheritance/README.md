# Inheritance and Polymorphism

- `parent class`, `super class`, `base class`
- `child class`, `derived class`

## Inheritance

มี 2 ไฟล์ `Noodle.java`, `Spaghetti.java`

์`Noodle` เป็น parent `Spaghetti` เป็น child

```java
class Spaghetti extends Noodle {

}
```

การสร้าง child object ใน parent

```java
// Noodle.java
  public static void main(String[] args) {
    Spaghetti spaghettiPomodoro = new Spaghetti();
  }
```

## Inherit the constructor

child ได้รับ method constructor จาก parent ด้วย

บางครั้งไม่ได้ต้องการแบบนั้น

```java
// child constructor
ChildClass() {
    // using parent constructor
    super(arg1, arg2);
}
```

```java
// child constructor
ChildClass() {
    // override parent
    this.field1 = 3;
}
```

## Access Modifiers

![access modifier](./images/access-modifiers-chart.png)

> image from codecademy

`Noodle` (parent), `Ramen` (child)

```java
// Noodle

public class Noodle {
    private String ingredients;
}
```

> `ingredients` เป็น `private` **Ramen** จะ access ตัวแปรนี้ใน **Noodle** ไม่ได้

การเพิ่ม keyword `final` จะเป็นการไม่ให้ child override

```java
// Noodel
  final public boolean isTasty() {

    return true;

  }
```

## Polymorphism

- child สามารถแชร์ information/behavior ของ parent มาได้
  - ผลไม้ สามารถทำน้ำผลไม้ได้ (parent)
  - ส้มเป็นผลไม้ (child)
  - เอาส้มไปทำน้ำผลไม้ได้ (`class Orange` ก็ถือเป็น `class Fruit`)

### Override

```java
// Spaetzle (child)
 @Override
  public void cook() {
      // overrid Noodle cook() method
  }
```

### Using a Child Class as its Parent Class

```java
Noodle ramen = New Ramen();
```

## เก็บ child Array & ArrayList

สามารถเก็บ child ที่มี parent เดียวกันใน array หรือ array list

```java
Monster dracula, wolfman, zombie1;

dracula = new Vampire();
wolfman = new Werewolf();
zombie1 = new Zombie();

Monster[] monsters = {dracula, wolfman, zombie1};

for (Monster monster : monsters) {

}
```
