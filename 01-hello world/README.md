# Java Programming Structure 

`HelloWorld.java`

ในไฟล์มี class `HelloWorld`

```java
public class HelloWorld {

}
```

class คือ single **concept**

`HelloWord` concept คือ print คำว่า hello world

แต่ละไฟล์มี 1 **primary class** ที่ตรงกับชื่อไฟล์

ใน class มี `main()` method

```java
public static void main(String[] args) {

}
```

## commenting code

```java
// single line comment

/*
multi-line comment
*/
```

## Semicolons and Whitespace

- ไม่ได้ indent sensitive เหมือน python
- **semicolons** `;` = end of statement
- curly brace `{ }` = scope of class/method

## Compilation

```bash
javac HelloWorld.java
```

จะได้เป็นไฟล์ `.class`

```bash
java HelloWorld
```

## My first Java Program

```java
public class HelloWorld {
    public static void main(String[] args) {
        // my first java program from scratch.
        System.out.println("Hello World, my name is chuan");
    }
}
```
