# Object-Oriented

## Introduction to class

The fundamental concept of object-oriented programming is the class.

- class
- instance / object

**class** คือ blueprint สำหรับ object

blueprint บอกถึงลักษณะต่างๆ ของ object เช่น `student` มีรหัสนักศึกษา มีเกรดเฉลี่ย

![oop](./img/diagram_of_an_object-01.png)

> img from codecademy

## Class Syntax

```java
public class Car {
    public static void main(String[] args) {

        // program tasks

    }

}
```

- `public` คือ access level modifier
- `main()` ฟังก์ชันจะถูกเรียกเมื่อ execute

## Constructors

**constructor** ใช้ สร้าง instance ของ class

define constructor ใน class

```java
public class Car {

    public Car() {
        // constructor
    }

    public static void main(String[] args) {

        // program tasks

    }

}
```

- invoke constructor ใน `main()`
- instance ของ class `Car` ตัวแปรชื่อ `ferrari`
- กำหนด type เป็น `Car` (type เหมือนกับ class name)
- การ invoke constructor ต้องมี `new`
  - ไม่ใส่ `new` จะ error

```java
public class Car {

    // constructor method
    public Car() {
        // constructor
    }

    // main method
    public static void main(String[] args) {

        // invoke the constructor
        Car ferrari = new Car();

    }

}
```

```java
System.out.println(ferrari);

// Car@6bc7c054
// Car = class
// @6bc7c054 = instance’s memory location
```

## Instance Fields

```java
public class Car {

    // declare field
    String color;

    // constructor method
    public Car() {
        // constructor
    }

    // main method
    public static void main(String[] args) {
    }

}
```

## Constructor Parameters

```java
public class Car {

    // declare field
    String color;
    int velocity;

    // constructor method
    public Car(String carColor, int carVelocity) {
        // constructor
        color = carColor;
        velocity = carVelocity;
    }

    // main method
    public static void main(String[] args) {
        Car ferrari = new Car("red", 250);
        System.out.println(ferrari.color);
    }

}
```

## Class Method

- `public` class อื่นสามารถเรียกใช้งาน method นี้ได้
- `void` no specific output

```java
public class Car {

    // declare field
    String color;
    int velocity;

    // constructor method
    public Car(String carColor, int carVelocity) {
        // constructor
        color = carColor;
        velocity = carVelocity;
    }

    // car method
    public void startEngine() {
        System.out.println("Starting the car!");
    }

    // main method
    public static void main(String[] args) {
        Car ferrari = new Car("red", 250);
        System.out.println(ferrari.color);
        // call method
        ferrari.startEngine();
    }

}
```

### scope

**scope** ของ method ถูกกำหนดด้วย `{ }`

ไม่สามารถเข้าถึงตัวแปรที่อยู่ใน method จากโค้ดที่อยู่นอก scope ของ method

```java
class Car {
  String color;
  int milesDriven;

  public void drive() {
     String message = "Miles driven: " + milesDriven;
     System.out.println(message);
  }

  public static void main(String[] args){
     Car myFastCar = new Car("red");
     myFastCar.drive();
  }
}
```

`message` ที่อยู่ใน `drive()` จะไม่สามารถเข้าถึงได้โดยโค้ดใน `main()`

`milesDriven` ที่ถูกประกาศใน class สามารถเข้าถึงได้ทั้งหมด

## Parameters

**scope** ป้องกันการเรียก variable ใน method จาก method อื่น

ถ้าต้องการส่งข้อมูลจาก method ไปยังอีก method สามารถใช้ **parameter**

## Return

keyword `void` หมายถึง `completely empty` คือ method ไม่มีค่าที่ต้อง return

กำหนดประเภทของค่าที่ต้อง return ได้เช่น `int`, `char`

```java
// return int type
public int funcName() {
    int someInt = 20200218;
    return someInt;
}
```

## `toString()`

เมื่อลอง print oject มันจะแสดงผลเป็น `class@mem_loc`

สามารถสร้าง method `toString()` เพื่อให้แสดงผลค่าที่ต้องการได้

```java
public String toString() {
       return "object detail...";
}
```
