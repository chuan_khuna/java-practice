public class Person {
String name;
String birthday;
String job;

  public Person(String personName, String personBirthday, String personJob) {
    System.out.println("Constructor invoked!");
    name = personName;
    birthday = personBirthday;
    job = personJob;
  }

  public String toString() {
    return "toString method: Person name " + name;
  }

  public void getJob() {
    System.out.println(job);
  }

  public static void main(String[] args) {
    System.out.println("Main method started");
    Person sawano = new Person("Sawano Hiroyuki", "12/09/1980", "music composer");
    System.out.println(sawano.name);
    System.out.println(sawano);
    sawano.getJob();
  }
}