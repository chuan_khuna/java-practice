# Loops

## While

`while` มีความคล้าย `if` ตรงที่จะทำ loop เมื่อเงื่อนไขเป็นจริง

```java
while (condition){

}
```

### incrementing while loop

```java
int i = 0;

while (i <= 10) {
    i++;
}
```

## For

- counter เริ่มต้น
- เงื่อนไข counter สิ้นสุด
- การเพิ่ม counter

```java
for (int i = 0; i <= 10; i += 1){

}
```

## For-Each

เหมือน `for item in items:` ใน python

`:` คือ `in`

```java
for (String trackname : tracklist){

}
```
