# Debugging

## Exception

```java
try {
    // ...
} catch (NullPointerException e) {
    System.err.println();
} catch (ArithmeticException e) {
    System.err.println();
}
```
