# Arrays

Array เก็บข้อมูล type เดียวกัน

index เริ่มที่ `0`

## Creating array

type ตามด้วย `[ ]`

```java
double[] price;
price = {0.5, 1.25, 0.75, 1.50, 2.75, 3.25};

String[] composers;
composers = {"SawanoHiroyuki", "Yuki Hayashi", "KOHTA YAMAMOTO"};
```

### importing array

เมื่อต้องการ print ตัวแปร array ให้อ่านง่าย

```java
import java.util.Arrays;

System.out.println(Arrays.toString(composers));
```

## Get element by index

```java
System.out.println(composers[0]);
```

## Empty Array

```java
String[] emptyStringArr = new String[10];
```

## Length

```java
array.length
```

## `String[] args`

ใน `main()` มี parameter แบบ `String[] args`

```bash
java HelloWorld arg1 arg2
```

# Array List

```java
import java.util.ArrayList;
```

## ArrayList

- เก็บสมาชิกที่ type เดียวกัน
- เข้าถึงโดยใช้ index
- เพิ่ม/ลบสมาชิกได้

array เพิ่มสมาชิกไม่ได้

```java
String[] tracklist_array = {"aliez", "no differences", "keep on keeping on"};
// tracklist 0 1 2

// error
tracklist_array[3] = "breath//less";
```

## Creating ArrayList

ใช้ `< >` เพื่อบอกประเภทข้อมูลที่ต้องการเก็บใน array list

```java
ArrayList<String> tracklist;
```

### empty array list

```java
ArrayList<String> tracklist = new ArrayList<String>();
```

### primitive type ใช้ใน array list ไม่ได้

- `int`: `<Integer>`
- `double`: `<Double>`
- `char`: `<Char>`

## Adding Item

```java
tracklist.add("ninelie");
```

## Removing Item

```java
// remove by index
tracklist.remove(1);

// remove by value
tracklist.remove("acyort");
```

## ArrayList size

```java
tracklist.size();
```

## Access by index

ถ้าเป็นประเภท Array สามารถใช้ `trackname[index]` ได้ แต่ใน ArrayList ต้องใช้ `trackname.get(index)`

```java
tracklist.get(index);
```

## Changing Value

```java
tracklist.set(0, "aLIEz");
```

## getting an item index

```java
tracklist.indexOf("aLIEz");
```
