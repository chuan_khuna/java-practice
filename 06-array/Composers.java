import java.util.Arrays;
import java.util.ArrayList;

public class Composers {
  public static void main(String[] args) {
    // my favorite composer
    String[] favoriteComposers = {"SawanoHiroyuki", "Yuki Hayashi", "KOHTA YAMAMOTO"};
    favoriteComposers[0] = "SawanoHiroyuki[nZk]";
    System.out.println(Arrays.toString(favoriteComposers));
    System.out.println("num composer: " + favoriteComposers.length);

    // array cannot add element
    // favoriteComposers[3] = "new composer";

    ArrayList<String> sawanoTracklist = new ArrayList<String>();
    sawanoTracklist.add("aLIEz");
    sawanoTracklist.add("no differences");
    System.out.println(sawanoTracklist);
  }
}