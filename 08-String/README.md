# String Methods

```java
str.length();

str1.concat(str2);
str1.equals(str2);
```

`indexOf()` ใช้หา index ของตัวแรกที่ตรงกับ substring ที่ต้องการได้

`indexOf()` จะ return index ที่เจอครั้งแรก

```java
str.indexOf(substring);
// str = "SawanoHiroyuki"
// substring = "Sawano"
// 0
```

`charAt()` return ตัวอักษรที่ index นั้น

```java
str.charAt(index);
```

`substring()` เหมือน python `string[start:end]`

```java
str.substring(begin_index, end_index);
```

```java
str.toLowerCase();
str.toUpperCase();
```
