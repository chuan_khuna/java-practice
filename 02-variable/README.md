# Variables

่่java ต้องบอก type ของ variable

Java เป็น **static typing**

`Primitive datatypes` (the simplest types of data) ประเภทข้อมูลดั้งเดิม

- `int`
- `double`
- `boolean`

```java
// declare variable
// type name
int age;
age = 23;

// declare and assign value
int age = 23;
```

## int

ข้อมูลจำนวนเต็ม -2,147,483,648 ถึง 2,147,483,647

## double

float

double สามารถเก็บจำนวนที่ใหญ่มากๆได้ ประมาณ `1.797 e+308`

## boolean

```java
boolean bool_true = true;
boolean bool_false = false;
```

## char

1 ตัวอักษร ใช้ **single quote** `' '`

```java
char grade = 'A';
char punctuation = '!';
```

## String

`String` เป็น **object** ไม่ใช่ **primitives** ใช้ **double quote** `" "`

```java
String name = "Chuan";
```

## Naming

- variable name **case sensitive**
- **camelCase**
- สามารถเริ่มด้วย `$` และ `_`
- เริ่มต้นด้วยตัวเลขไม่ได้

## Data type CheatSheet

### Primitive vs Non-primitive

#### Primitive

##### boolean type

- `boolean`

##### numeric type

- **character value**

  - `char`

- **integral value**
  - **integer**
    - `byte`
    - `short`
    - `int`
    - `long`
  - **floating point**
    - `float`
    - `double`

#### Non-Primitive

- `String`
- `Array`
- etc.

## Data Type Range

| type    | size(bit) | range                               |
| ------- | --------- | ----------------------------------- |
| boolean | 1         | `true`, `false`                     |
| char    | 16        | ASCII                               |
| short   | 16        | `-32,768` to `32,767`               |
| int     | 32        | `-2,147,483,648` to `2,147,483,647` |
| long    | 64        | `-2^63` to `2^63 - 1`               |
| float   | 32        | 6-7 decimal digit                   |
| double  | 64        | 15 decimal digit                    |

- [w3schools](https://www.w3schools.com/java/java_data_types.asp)
- [geeksforgeeks](https://www.geeksforgeeks.org/data-types-in-java/)
