# Conditionals and Control Flow

## if-then-else

if ตามด้วย `( )` ในวงเล็บเป็นประเภท boolean

```java
if (condition) {

} else if (another_condition) {

} else {

}
```

## switch

`switch` เป็น alternative ของ if then else

เป็นการตรวจตัวแปรในวงเล็บหลัง `switch` ว่าเหมือนกับค่าใน `case` ไหน

```java
switch (variable) {
    case value_1:
        // ...
        break;

    case value_2:
        // ...
        break;

    default:
        // else
}
```

## Conditional Operators

- `&&` AND
- `||` OR
- `!` NOT

```java
if (!variable) {
    // ...
} else {
    // ...
}
```
